import Axios from 'axios';
import Qs from 'qs';
import { rsaDecryptByPublicKey, rsaPublicData } from '@/utils/rsa.js';
import Aes from '@/utils/aes.js';
import { useUserStore } from '@/store';
import { ElMessage } from 'element-plus';

const request = Axios.create({
  timeout: 100000,
  // timeout: 20000, // 请求超时 20s
});

let showMsg = true;

// 用于存储pending的请求的数组（处理多条相同请求）
const pendingRequest = new Map();

// 生成request的唯一key
const generateRequestKey = (config = {}) => {
  // 通过url，method，params，data生成唯一key，用于判断是否重复请求
  // params为get请求参数，data为post请求参数
  const { url, method, params, data } = config;
  return [url, method, Qs.stringify(params), Qs.stringify(data)].join('&');
};

// 将重复请求添加到pendingRequest中
const addPendingRequest = (config) => {
  const key = generateRequestKey(config);
  if (!pendingRequest.has(key)) {
    config.cancelToken = request.CancelToken((cancel) => {
      pendingRequest.set(key, cancel);
    });
  }
};

// 取消重复请求
const removePendingRequest = (config) => {
  const key = generateRequestKey(config);
  if (pendingRequest.has(key)) {
    const cancelToken = pendingRequest.get(key);
    cancelToken(key); // 取消之前发送的请求
    pendingRequest.delete(key); // 请求对象中删除requestKey
  }
};

// 请求拦截器（发起请求之前的拦截）
request.interceptors.request.use(
  (config) => {
    removePendingRequest(config);
    addPendingRequest(config);

    console.log('1111111', config);
    if (config.url == '/oauth/login') {
      // 只有登录接口加密
      let randomKey = Aes.randomString(32);
      let paramsData = Aes.encryptAes(JSON.stringify(config.data), randomKey);
      let encrypttoken = rsaPublicData(randomKey);
      config.data = paramsData;
      config.headers['encrypttoken'] = encrypttoken;
      config.headers['encrypt'] = true;
    }

    // 关于formData入参的处理
    // config.headers['Content-Type'] === 'application/x-www-form-urlencoded' &&
    //   (config.data = Qs.stringify(config.data));
    /**
     * 根据你的项目实际情况来对 config 做处理
     * 这里没有对 config 不做任何处理，直接返回
     */
    // 这里还可以添加token等等
    config.headers['System-Name'] = 'Pi-Diginn'; // 要改成一样才能公用token
    //Windchill的接口
    if (config.url.includes('Windchill')) {
      const subAppToken = window.$Cookies.get('subAppToken');
      const userInfo = window.$Cookies.get('userInfo') ? JSON.parse(window.$Cookies.get('userInfo')) : {};
      if (subAppToken) {
        config.headers.Authorization = subAppToken;
        config.headers['username'] = userInfo.username;
      }
    } else {
      const userStore = useUserStore();
      //非Windchill的接口
      config.headers.Authorization = userStore.authorToken || window.$Cookies.get('authorToken') || undefined;
    }
    return config;
  },
  (error) => Promise.reject(error),
);

// 响应拦截器（获取到响应时的拦截）
request.interceptors.response.use(
  (response) => {
    console.log('2222222', response);
    removePendingRequest(response.config);
    
    const headers = response.headers;
    // console.log('headers', typeof headers['refresh-token']);
    headers['refresh-token'] && window.$Cookies.set('authorToken', headers['refresh-token'], { expires: 1 }); //替换新的token
    headers['content-disposition'] && sessionStorage.setItem('downloadName', headers['content-disposition']); //批量下载的文件名
    headers['message'] && sessionStorage.setItem('MessageError', headers['message']); //批量下载的文件名

    const data = response.data;
    // 解密data
    if (response.headers.encrypt === 'true') {
      let AesKey = rsaDecryptByPublicKey(response.headers.encrypttoken);
      response.data = Aes.decryptAes(data.data, AesKey);
    }

    //token过期自动退出登录
    if (data.code == 401) {
      if (showMsg) {
        ElMessage({
          message: data.message.includes('Token') ? '请重新登录！' : data.message,
          type: 'error',
        });
        showMsg = false;
        setTimeout(() => {
          showMsg = true;
        }, 3000);
      }
      const userStore = useUserStore();
      userStore.logout();
      return;
    }

    // const { code, data, message } = response.data;
    // if (code === 200) return data;
    // else if (code === 401) {
    //   // jumpLogin();
    // } else {
    //   Message.error(message);
    //   return Promise.reject(response.data);
    // }
    /**
     * 根据你的项目实际情况来对 response 和 error 做处理
     * 这里对 response 和 error 不做任何处理，直接返回
     */
    // if (response.data.success) {
    //   return response.data;
    // }

    // 弹出提示
    return data;
  },
  (error) => {
    console.log('44444444', error);
    removePendingRequest(error.config || {});

    if (error.response && error.response.data) {
      if (showMsg) {
        const code = error.response.status;
        const msg = error.response.data.message;
        window.$message.error(`Code: ${code}, Message: ${msg}`);
        showMsg = false;
        setTimeout(() => {
          showMsg = true;
        }, 3000);
      }
    } else {
      //取消接口的情况无需把错误抛出
      error.code !== 'ERR_CANCELED' && window.$message.error(`${error}`);
    }
    return Promise.reject(error);
  },
);

/**
 * get请求方式
 * @url {string} 请求的接口路径
 * @params {object} 入参
 * @headers {object} 通常是headers里的一些特殊处理
 * */
export function get(url, params, headers) {
  return request({
    url,
    method: 'get',
    params,
    // ...headers,
    headers: headers || {
      'Content-Type': 'application/json;charset=UTF-8',
    },
  });
}

/**
 * post请求方式
 * @url {string} 请求的接口路径
 * @data {object} 入参
 * @headers {object} 通常是headers里的一些特殊处理
 * */
export function post(url, data, headers) {
  return request({
    url,
    method: 'post',
    data,
    // ...headers,
    headers: headers || {
      'Content-Type': 'application/json;charset=UTF-8',
    },
  });
}
let fn = () => {};
/**
 * post请求方式。取消上一次请求
 * @url {string} 请求的接口路径
 * @data {object} 入参
 * */
export function postNoRepeat(url, data) {
  fn();
  return request({
    url,
    method: 'post',
    data,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    },
    //取消该接口的上一次请求
    cancelToken: new Axios.CancelToken((cancelFn) => {
      fn = cancelFn;
    }),
  });
}

/**
 * 下载常用
 * @param {string} url
 * @param {object} data
 * @param {object} params
 */
export function download(url, params, headers) {
  return request({
    method: 'post',
    url,
    data: params,
    responseType: 'blob',
    headers: headers || {
      'Content-Type': 'application/json;charset=UTF-8',
    },
  });
}

/**
 * 文件预览常用
 * @param {string} url
 * @param {object} data
 * @param {object} params
 */
export function filePreview(url, params) {
  return request({
    method: 'post',
    url,
    data: params,
    responseType: 'blob',
  });
}

/**
 * 上传常用
 * @param {string} url
 * @param {object} data
 * @param {object} params
 */
export function upload(url, params) {
  return request({
    method: 'post',
    url,
    data: params,
    headers: {
      'Content-Type': 'multipart/form-data; boundary=---011000010111000001101001',
    },
  });
}
